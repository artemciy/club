package ru.glim.club;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class SidekickMedia extends Service {
  volatile Thread thread;
  volatile boolean stop = false;

  private static final String CHANNEL_ID = "Sidekick";
  private static final int NOTIFICATION_ID = 1427858071;  // Math.floor (Math.random() * 2147483647)

  Notification notification() {
    Intent intent = new Intent (this, MainActivity.class);
    PendingIntent pendingIntent = PendingIntent.getActivity (this, NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    // https://developer.android.com/training/notify-user/channels#CreateChannel
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel channel = new NotificationChannel (CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
      channel.setDescription (CHANNEL_ID);
      NotificationManager notificationManager = getSystemService (NotificationManager.class);
      notificationManager.createNotificationChannel (channel);
    }

    // cf. https://developer.android.com/training/wearables/notifications/creating
    NotificationCompat.Builder nb = new NotificationCompat.Builder (this, CHANNEL_ID)
      .setChannelId (CHANNEL_ID)
      .setSmallIcon (R.drawable.launch_background)
      .setContentIntent (pendingIntent)
      .setAutoCancel (false)
      .setOngoing (true);  // Stay awake.

    Object speech = "TBD";
    if (speech == null) nb
      .setLocalOnly (true)
      .setCategory (NotificationCompat.CATEGORY_SERVICE)
      .setPriority (NotificationCompat.PRIORITY_DEFAULT)
      .setContentTitle ("Sidekick online");
    else nb
      .setLocalOnly (false)  // Would like it on Wear OS.
      .setCategory (NotificationCompat.CATEGORY_MESSAGE)
      .setPriority (NotificationCompat.PRIORITY_MAX)
      .setContentText (speech.toString())
      .setTicker (speech.toString())
      .setStyle (new NotificationCompat.BigTextStyle().bigText (speech.toString()));

    Notification notification = nb.build();
    return notification;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    Notification notification = notification();
    NotificationManagerCompat.from (this) .notify (NOTIFICATION_ID, notification);

    startForeground (NOTIFICATION_ID, notification);

    if (thread == null) thread = new Thread (new Runnable() {
      @Override
      public void run() {
        for (;;) {
          if (stop) break;
          System.out.println("SidekickMedia] Hey, I'm still out and about.");
          try {
            Thread.sleep (1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }, "SidekickMedia");
    return super.onStartCommand(intent, flags, startId);
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    System.out.println("SidekickMedia#onBind");
    return null;
  }

  @Override
  public void onDestroy() {
    stop = true;
    NotificationManagerCompat.from (this) .cancel (NOTIFICATION_ID);
    super.onDestroy();
  }
}
