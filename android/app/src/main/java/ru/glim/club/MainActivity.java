package ru.glim.club;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.BasicMessageChannel;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private EventChannel logC;
  private EventChannel.EventSink logSink;

  /** Helps “flutter drive” with checking on the Android notifications */
  private void checkIfNotificationExists() {
    if (logSink == null) throw new RuntimeException ("!logSink");

    // https://developer.android.com/reference/android/app/NotificationManager#getActiveNotifications()
    final NotificationManager notificationManager = (NotificationManager) getSystemService (NOTIFICATION_SERVICE);
    final StatusBarNotification[] notifications = notificationManager.getActiveNotifications();
    boolean found = false;
    logSink.success ("checkIfNotificationExists] listing notifications...");
    for (StatusBarNotification notification: notifications) {
      logSink.success ("checkIfNotificationExists] notification " + notification.getId());
      if (notification.getId() == 1061155033) found = true;}
    if (!found) throw new RuntimeException ("Notificaion with ID 1061155033 is not in the list");}

  private void createChannels() {
    final BinaryMessenger bm = getFlutterEngine().getDartExecutor().getBinaryMessenger();
    final MainActivity self = this;

    EventChannel chan = new EventChannel (bm, "club.glim.ru/logC");
    chan.setStreamHandler (new EventChannel.StreamHandler() {
      @Override
      public void onListen (Object arguments, EventChannel.EventSink eventSink) {
        logSink = eventSink;
        createNotification();}
      @Override
      public void onCancel (Object arguments) {
        logSink = null;}});
    logC = chan;

    // cf. https://flutter.io/docs/development/platform-integration/platform-channels
    // cf. https://api.flutter.dev/javadoc/io/flutter/plugin/common/MethodChannel.Result.html
    new MethodChannel (bm, "club.glim.ru/platform") .setMethodCallHandler (new MethodChannel.MethodCallHandler() {
      @Override
      public void onMethodCall (MethodCall call, MethodChannel.Result result) {
        if (call.method.equals ("checkIfNotificationExists")) {

          try {
            checkIfNotificationExists();
            result.success ("{\"exists\": true}");
          } catch (Throwable ex) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final PrintStream ps = new PrintStream (baos);
            ex.printStackTrace (ps);
            final Charset utf8 = StandardCharsets.UTF_8;
            String trace = null;
            try {trace = baos.toString (utf8.name());} catch (UnsupportedEncodingException uee) {}
            result.error ("!checkIfNotificationExists", ex.toString(), trace);}

        } else if (call.method.equals ("createNotification")) {
          // Setup a persistent notification in order to stay in memory

          Intent serviceIntent = new Intent (self, SidekickMedia.class);
          startService (serviceIntent);

        } else if (call.method.equals ("cancelNotification")) {

          //NotificationManagerCompat.from (SELF) .cancel (NOTIFICATION_ID);

        } else {  // Unknown method

          System.out.println ("onMethodCall] method: " + call.method);
          result.notImplemented();}}});}

  /**
   * For API versions before Oreo (26) we can simply skip this function.
   * cf. https://stackoverflow.com/questions/54390584/android-notificationchannel-compatability-with-older-api
   */
  @RequiresApi(26)
  private void createNotificationChannel(String channelId) {
    if (logSink == null) throw new RuntimeException ("!logSink");

    final NotificationChannel channel = new NotificationChannel (channelId, "NotificationChannel name", NotificationManager.IMPORTANCE_DEFAULT);
    channel.setDescription ("NotificationChannel description");

    final NotificationManager notificationManager = (NotificationManager) getSystemService (NOTIFICATION_SERVICE);
    notificationManager.createNotificationChannel (channel);}

  private void createNotification() {
    if (logSink == null) throw new RuntimeException ("!logSink");

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      createNotificationChannel ("ru.glim.club/notification");}

    final NotificationCompat.Builder builder = new NotificationCompat.Builder (this, "ru.glim.club/notification")
      .setSmallIcon (R.drawable.launch_background)  // cf. https://documentation.onesignal.com/docs/customize-notification-icons
      .setContentTitle ("Test notification")
      .setContentText ("Test text")
      .setPriority (NotificationCompat.PRIORITY_DEFAULT);

    final NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
    // notificationId is a unique int for each notification that you must define
    final int notificationId = 1061155033;  // Math.floor (Math.random() * 2147483647)
    notificationManagerCompat.notify (notificationId, builder.build());}

  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
    GeneratedPluginRegistrant.registerWith(flutterEngine);
    createChannels();}}
