import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_tts/flutter_tts.dart';
//import 'package:proximity_plugin/proximity_plugin.dart';

import 'src/bot.dart';
//import 'src/hash.dart';

enum TtsState {playing, stopped}

/// A piece of text that we want to be voiced in the app.
class Speech {
  /// A text to read aloud.
  String text;
  /// Unicode emoji reactions attached to the text.
  String reactions;
  /// Time when the entry was touched, in seconds since UNIX epoch, server time.
  int lm;
  /// Time when the entry was obtained from server, in milliseconds since UNIX epoch, client time.
  int synced;
  Speech (this.text, this.reactions, this.lm, this.synced);}

class _ClubState extends State<ClubApp> with WidgetsBindingObserver, BotHandler {
  FlutterTts _flutterTts;
  dynamic _languages;
  dynamic _voices;
  String _language;
  String _voice;
  double _volume = 1.0;
  List<Speech> _speech = [];
  int _serverTimeDeltaMs = 0;  // = local time - server time
  int _lastAlarmMs = 0;
  static const _PLATFORM = const MethodChannel ('club.glim.ru/platform');
  Bot _bot;
  /// The text being displayed and pronounced currently
  String _eventText = '';
  TtsState ttsState = TtsState.stopped;
  final GlobalKey<AnimatedListState> _animatedScheduleList = GlobalKey();
  final _textInputController = new TextEditingController();
  final _secStor = new FlutterSecureStorage();
  /// Allows the native code to log with Flutter
  static const EventChannel _logC = EventChannel ('club.glim.ru/logC');

  get isPlaying => ttsState == TtsState.playing;
  get isStopped => ttsState == TtsState.stopped;

  Future<dynamic> _platform (String method, [Map<String, dynamic> args]) async {
    try {
      return await _PLATFORM.invokeMethod (method, args);
    } on MissingPluginException catch (ex) {
      // ^^ We sometimes get this while reloading the app in the developlment mode
      print ('_PLATFORM $method: $ex');}}

  void _onLogC (Object line) {print ('native] $line');}

  @override initState() {
    super.initState();
    _logC.receiveBroadcastStream().listen (_onLogC, onError: _onLogC);
    _clubs.add (this);
    _bot = Bot (this);
    initTts();
//    _platform ('createNotification');
    // Suspend an ongoing alarm when a hand is moved over the proximity sensor.
    //proximityEvents.listen ((ProximityEvent event) {suspendAlarm();});
    WidgetsBinding.instance.addObserver (this);}

  @override void reassemble() {
    super.reassemble();
    _bot?.reassemble();}

  @override void dispose() {
    super.dispose();
    _clubs.remove (this);
    _bot.close();
    _flutterTts.completionHandler = (){};
    _flutterTts.stop();
    //print ('dispose] Disposing of notification...');
    _platform ('cancelNotification');
    WidgetsBinding.instance.removeObserver (this);}

  @override void didChangeAppLifecycleState (AppLifecycleState state) {
    //print ('didChangeAppLifecycleState] $state');
  }

  @override void didHaveMemoryPressure() {print ('didHaveMemoryPressure');}

  void stopAlarm() {
    if (_lastAlarmMs != 0) {
      FlutterRingtonePlayer.stop();
      _lastAlarmMs = 0;}}

  void suspendAlarm() {
    if (_lastAlarmMs != 0) {
      FlutterRingtonePlayer.stop();
      // `botTick` would replay the alarm after ~55 seconds.
      _lastAlarmMs = new DateTime.now().millisecondsSinceEpoch;}}

  @override botTick() {
    final nowMs = new DateTime.now().millisecondsSinceEpoch;
    Set<int> timeframes = Set();  // Matching timeframes.
    Set<int> expired = Set();  // Expired timeframes.
    Set<int> alarms = Set();  // Expired timeframes with alarms.
    for (var ih = 0; ih < _speech.length; ++ih) {
      final sp = _speech[ih];
      var timeframe = 0;
      if (sp.reactions.contains ('⌛')) timeframe += 25 * 60;  // :hourglass:
      if (sp.reactions.contains ('⏳')) timeframe += 25 * 60;  // :hourglass_flowing_sand:
      // NB: These numbers are formed by adding the COMBINING ENCLOSING KEYCAP (E2 83 A3) to a normal number,
      // we can process them up to nine by checking for that suffix.
      if (sp.reactions.contains ('1⃣')) timeframe += 60 * 60;  // :one:
      if (sp.reactions.contains ('2⃣')) timeframe += 120 * 60;  // :two:
      final serverTimeMs = nowMs - _serverTimeDeltaMs;
      final age = serverTimeMs / 1000 - sp.lm;
      if (0 < timeframe && timeframe < age) {
        if (sp.reactions.contains ('⏰')) alarms.add (ih);
        expired.add (ih);
      } else if (timeframe > 0) timeframes.add (ih);}

    StringBuffer text = new StringBuffer();
    for (var ih = 0; ih < _speech.length; ++ih) {
      final sp = _speech[ih];

      // If there are active timeframes and `sp` is not timeframed then skip it.
      if (timeframes.isNotEmpty && !timeframes.contains (ih)) continue;

      if (expired.contains (ih)) continue;

      text.writeln (sp.text);}

    if (alarms.isNotEmpty && isPlaying) {
      if ((nowMs - _lastAlarmMs) / 1000 > 55) {
        _lastAlarmMs = nowMs;
        FlutterRingtonePlayer.playAlarm (volume: 0.1);}
    } else stopAlarm();

    final textZ = text.toString();

    if (textZ != _eventText) setState(() {
      //print ('udpHandle] setting event text to ' + struct['speech']);
      _eventText = textZ;
      _platform ('createNotification', {'speech': _eventText});
      if (ttsState == TtsState.stopped) speak();});}

  @override botHandle (dynamic struct) {
    if (struct['voice'] != null && struct['voice'] != _voice) setState(() {
      //print ('udpHandle] setting voice to ' + struct['voice']);
      _voice = struct['voice'];});

    final nowMs = new DateTime.now().millisecondsSinceEpoch;
    if (struct['now_ms'] != 0) _serverTimeDeltaMs = nowMs - struct['now_ms'];

    if (struct['speech'] != null) {
      // Bot-programmable and bot-directed?
      //flutterTts.setVolume (struct['volume'] != null ? struct['volume'] : DEFAULT_VOLUME);

      List speech = struct['speech'];

      // Add new entries to `_speech` and update the existing ones.
      for (var ix = 0; ix < speech.length; ++ix) {
        final en = speech[ix] as Map;
        final sp = Speech (en['text'], en['reactions'], en['lm'], nowMs);
        Speech have = _speech.firstWhere ((he) => he.text == sp.text, orElse: () => null);
        if (have != null) {
          have.reactions = sp.reactions;
          have.lm = sp.lm;
          have.synced = nowMs;
        } else {
          _speech.add (sp);}}

      // Detect removed entries.
      for (var ih = _speech.length - 1; ih >= 0; --ih) {
        final have = _speech[ih];
        if (have.synced == nowMs) continue;
        _speech.removeAt (ih);}}}

  void _setVolume() {
    // 0 .. 8 -> 0 .. 0.2
    // 8 .. 10 -> 0.2 .. 1
    double sv = _volume < 7.0
      ? _volume / 7.0 * 0.2
      : 0.2 + (_volume - 7.0) / 3.0 * 0.8;
    //print ('_setVolume] $_volume -> $sv');
    _flutterTts.setVolume (sv);}

  initTts() {
    _flutterTts = FlutterTts();
    _setVolume();
    _flutterTts.setPitch (1.1);
    _flutterTts.setSpeechRate (0.3);

    if (Platform.isAndroid) {
      _getLanguages();
      _getVoices();
    } else if (Platform.isIOS) {
      _getLanguages();}

    _flutterTts.setStartHandler(() {
      setState(() {
        ttsState = TtsState.playing;});});

    _flutterTts.setCompletionHandler(() {
      setState(() {
        ttsState = TtsState.stopped;});});

    _flutterTts.setErrorHandler((msg) {
      setState(() {
        ttsState = TtsState.stopped;});});}

  Future _getLanguages() async {
    _languages = await _flutterTts.getLanguages;
    if (_languages != null) setState(() => _languages);}

  Future _getVoices() async {
    _voices = await _flutterTts.getVoices;
    if (_voices != null) setState (() => _voices);}

  Future speak() async {
    if (_eventText != null && _eventText.isNotEmpty) {
      Function stateSetter = (result) {if (result == 1) setState (() => ttsState = TtsState.playing);};
      _flutterTts.completionHandler = () {  // Keep repeating the text.
        Future.delayed (const Duration (seconds: 2), () {  // The pause between the repeats.
          _flutterTts.speak (_eventText) .then (stateSetter);});};
      _flutterTts.speak (_eventText) .then (stateSetter);}}

  Future onPlayPressed() async {
    return speak();}

  Future stop() async {
    _flutterTts.completionHandler = (){};
    var result = await _flutterTts.stop();
    if (result == 1) setState(() => ttsState = TtsState.stopped);
    stopAlarm();}

  List<DropdownMenuItem<String>> getLanguageDropDownMenuItems() {
    var items = List<DropdownMenuItem<String>>();
    for (String type in _languages) {
      items.add(DropdownMenuItem(value: type, child: Text(type)));}
    return items;}

  List<DropdownMenuItem<String>> getVoiceDropDownMenuItems() {
    var items = List<DropdownMenuItem<String>>();
    for (String type in _voices) {
      items.add(DropdownMenuItem(value: type, child: Text(type)));}
    return items;}

  void changedLanguageDropDownItem(String selectedType) {
    setState(() {
      _language = selectedType;
      _flutterTts.setLanguage(_language);});}

  void changedVoiceDropDownItem(String selectedType) {
    setState(() {
      _voice = selectedType;
      _flutterTts.setVoice(_voice);});}

  Widget volumeCtrl() => Slider (
    value: _volume,
    min: 0.0,
    max: 10.0,
    onChanged: (v) => setState (() {
      _volume = v;
      _setVolume();
      // The volume will not be applied until we restart the record.
      speak();}));

  void _onNewTextInput (String input) {
    input = input.trim();
    if (input.isNotEmpty) {
      // TBD: Move asynchronous code into a model with a synchronous facade
      () async {
        final res = await botRequest ({'text_input': input});
        if (input.contains ('bind app ') && res['key'] is int && res['secret'] is String) {
          final key = BotKey.fromJson (res);
          await _secStor.write (key: 'bot_key', value: json.encode (key.toJson));
          _bot.botKeyCached = false;}
      } ();
    }
    _textInputController.clear();}

  Future<BotKey> botKey() async {
    String s = await _secStor.read (key: 'bot_key');
    if (s == null) return null;
    return BotKey.fromJson (json.decode (s));}

  Widget textInputSection() => TextField (
    key: Key ('textInputSection'),
    autofocus: true,
    autocorrect: false,
    controller: _textInputController,
    decoration: InputDecoration (
      prefixText: '/r '),
    onSubmitted: _onNewTextInput);

  Widget btnSection() => Container (
      padding: EdgeInsets.only (top: 5.0),
      child: Row (mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        _buildButtonColumn (
            Colors.green, Colors.greenAccent, Icons.play_arrow, 'PLAY', onPlayPressed),
        _buildButtonColumn (
            Colors.red, Colors.redAccent, Icons.stop, 'STOP', stop)]));

  Widget languageDropDownSection() => Container(
      padding: EdgeInsets.only(top: 50.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        DropdownButton(
          value: _language,
          items: getLanguageDropDownMenuItems(),
          onChanged: changedLanguageDropDownItem)]));

  Widget voiceDropDownSection() => Container(
      padding: EdgeInsets.only(top: 50.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        DropdownButton(
          value: _voice,
          items: getVoiceDropDownMenuItems(),
          onChanged: changedVoiceDropDownItem)]));

  Column _buildButtonColumn(Color color, Color splashColor, IconData icon,
      String label, Function func) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
              icon: Icon(icon),
              color: color,
              splashColor: splashColor,
              onPressed: () => func())]);}

  Widget _removedScheduleItem (BuildContext ctx, Animation<double> animation) {
    return _buildScheduleItem (ctx, 0, animation);}

  Widget _buildScheduleItem (BuildContext ctx, int idx, Animation<double> animation) {
    return SizeTransition (sizeFactor: animation, child: Column (
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text ('qwe $idx'),
        ButtonTheme (
          minWidth: 0,
          height: 0,
          padding: EdgeInsets.symmetric (vertical: 6, horizontal: 11),
          // Keeps the height of the bar independent from the width of the padding.
          layoutBehavior: ButtonBarLayoutBehavior.constrained,
          // Shrinks the in-between space.
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          buttonColor: Color.fromRGBO (255, 255, 255, 1),
          child: ButtonBar (
            children: [
              RaisedButton (onPressed: (){}, child: const Text ('🔁')),
              RaisedButton (child: Text ('✂️'), onPressed: (){
                print ('Removing index $idx');
                _animatedScheduleList.currentState.removeItem (idx, _removedScheduleItem);}),
              RaisedButton (onPressed: (){}, child: Text ('✅'))]))]));}

  @override Widget build (BuildContext context) {
    return MaterialApp (
      home: Scaffold (
        appBar: AppBar (
          title: Text ('Club of queer trades')),
        body: new Container (
          //padding:  new EdgeInsets.all (8.0),
          child: new Column (
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              textInputSection(),
              btnSection(),
              volumeCtrl(),
              Expanded (child: AnimatedList (
                key: _animatedScheduleList,
                padding: const EdgeInsets.all (8.0),
                initialItemCount: 9,
                itemBuilder: (context, i, animation) {
                  return _buildScheduleItem (context, i, animation);}))],),)));}}

class ClubApp extends StatefulWidget {
  @override _ClubState createState() => _ClubState();}

/// Makes it easier to reach the app state
List<_ClubState> _clubs = [];

Future<String> driveHandler (String js) async {
  Map<String, dynamic> output = {};
  final Map<String, dynamic> input = json.decode(js);
  final String method = input['method'];

  if (method == '_onNewTextInput') {
    for (_ClubState club in _clubs) club._onNewTextInput (input['text']);
  } else if (method == 'checkIfNotificationExists') {
    for (_ClubState club in _clubs) {
      output = json.decode (await club._platform ('checkIfNotificationExists'));}
  } else throw new Exception ('Unknown method: $method');

  return json.encode (output);}

// As of 2019-11-03 the asynchronous version of `main` - `Future<void> main() async` - would SIGSEGV.

void main() {
  runZoned<void> (
    () {runApp (ClubApp());},
    onError: (exception, stackTrace) {reportError (exception, stackTrace);});}

