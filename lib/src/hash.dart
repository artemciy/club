import 'package:crypto/crypto.dart';
import 'package:convert/convert.dart';
import 'dart:convert';
import 'dart:math';

/// SHA-512 hash with key stretching,
/// where the number of iterations is limited by time,
/// allowing faster hardware to produce a more secure hash
/// while staying withing the UX time constraints.
class Sha512TimedHash {
  List<int> _hash;
  List<int> _salt;
  int _iterations;

  Sha512TimedHash({List<int> hash, List<int> salt, int iterations}) {
    this._hash = hash;
    this._salt = salt;
    this._iterations = iterations;
  }

  toString() {
    var hash = Digest(_hash).toString();
    var salt = Digest(_salt).toString();
    return "$hash;$salt;$_iterations";
  }

  /// @param pepper A random secret stored separately from the password,
  ///               preferably on another host, and being hard for an attacker to obtain.
  ///               Use an empty pepper if secure pepper is not available.
  static Sha512TimedHash pwHash(String pw, String pepper, int seconds) {
    assert(seconds > 0);
    var microseconds = seconds * 1000000;
    var timePassed = 0;

    var random = Random.secure();
    var salt =
        List<int>.generate(32 + random.nextInt(16), (i) => random.nextInt(256));
    var saltIdx = 0;

    var digest = Digest(utf8.encode(pw + pepper));

    var started = new DateTime.now();
    var iterations = 0;
    var safeIterationTimeframe = 200000;
    for (; timePassed < microseconds; ++iterations) {
      // Add a few bit of randomness into every iteration.
      digest.bytes[0] = digest.bytes[0] ^ salt[saltIdx];
      if (++saltIdx >= salt.length) saltIdx = 0;

      digest = sha512.convert(digest.bytes);
      // We're calculating time in small spans in order to be less affected by time adjustment jumps.
      // If delta is more than safeIterationTimeframe ms then it might be a jump into the future.
      // If the delta is negative then we jumped into the past.
      var delta = new DateTime.now().difference(started).inMicroseconds;
      // NB: In a browser (Flutter web) the time resolution might be limited to milliseconds.
      var minSpan = 9000;
      if (delta < 0 || delta > min (minSpan, safeIterationTimeframe / 2)) {
        // We don't want the accidental forward time jumps to reduce the strength of the hash.
        if (delta < safeIterationTimeframe) timePassed += delta;
        started = new DateTime.now();
      }
      if (delta > safeIterationTimeframe)
        safeIterationTimeframe = (safeIterationTimeframe * 4 + delta) ~/ 5;
    }

    return Sha512TimedHash(
        hash: digest.bytes, salt: salt, iterations: iterations);
  }

  static bool pwValidate(String pw, String pepper, String hash) {
    var split = hash.split(';');
    var h = Sha512TimedHash(
        hash: hex.decode(split[0]),
        salt: hex.decode(split[1]),
        iterations: int.parse(split[2]));
    assert("$h" == hash);
    return h.validate(pw, pepper);
  }

  bool validate(String pw, String pepper) {
    assert(_iterations > 0);

    var saltIdx = 0;

    var digest = Digest(utf8.encode(pw + pepper));

    for (var iterations = 0; iterations < _iterations; ++iterations) {
      digest.bytes[0] = digest.bytes[0] ^ _salt[saltIdx];
      if (++saltIdx >= _salt.length) saltIdx = 0;

      digest = sha512.convert(digest.bytes);
    }
    return Digest(_hash) == digest;
  }
}
