import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
//import 'package:workmanager/workmanager.dart';

/// The server should wrap up a long-polling response after that many seconds.
const POLL_WAIT_SEC = 22;

bool get inDebugMode {bool inDebugMode = false; assert (inDebugMode = true); return inDebugMode;}

class BotRequestEx implements Exception {
  String err; BotRequestEx (this.err);
  @override String toString() {return 'BotRequestEx ($err)';}}

Future<Map<String, dynamic>> botRequest (Map<String, dynamic> request, [String ifNoneMatch]) async {
  const url = 'https://dice.glim.ru/-api-/sidekick_handler';
  //const url = 'https://httpbin.org/post';
  final req = json.encode (request);
  final headers = {
    'Cache-Control': 'no-cache',
    'Content-Type': 'application/json'};
  if (ifNoneMatch != null) headers['If-None-Match'] = '"$ifNoneMatch"';
  final response = await http.post (url, body: 'mobile:$req', headers: headers);
  if (response.statusCode == 304) return null;
  if (response.statusCode != 200) {throw BotRequestEx ('status != 200: ${response.statusCode} ${response.reasonPhrase}');}
  final ct = response.headers['content-type'];
  if (ct != 'application/json') {throw BotRequestEx ('!application/json: $ct');}
  // NB: `http` fails to recognize that 'application/json' is UTF-8 by default.
  final body = Utf8Decoder().convert (response.bodyBytes);
  Map<String, dynamic> res = json.decode (body);

  final etag = response.headers['etag'];
  if (etag != null) {
    if (etag.startsWith ('"') && etag.endsWith ('"')) res['ETag'] = etag.substring (1, etag.length - 1);
    // CloudFlare adds the "weak validator" directive.
    else if (etag.startsWith ('W/"') && etag.endsWith ('"')) res['ETag'] = etag.substring (3, etag.length - 1);}

  return res;}

/// The file name and the line number from the current stacktrace.
class Loc {
  String file; int line;
  Loc (int frame) {
    final trace = StackTrace.current.toString();
    // #9      _runUserCode  (package:Sidekick/src/bot.dart:11:23)
    final re = new RegExp ('^#$frame' + r'(.*?)package:Sidekick/(src/)?(.*?)\.dart:(\d+):\d+\)$',
      caseSensitive: false, multiLine: true);
    final fm = re.firstMatch (trace);
    if (fm != null) {
      file = fm.group (3);
      line = int.parse (fm.group (4));}}
  @override String toString() {return file == null || file.isEmpty ? '' : '$file:$line] ';}}

void log (String line) {
  final loc = Loc (2);
  print ('mobileˡ] $loc$line');  // The prefix is similar to the one logged server-side.
  final _ = botRequest ({'log': line});}

Future<void> reportError (dynamic exception, dynamic stackTrace) async {
  print ('_reportError] $exception');
  if (inDebugMode) {print (stackTrace); return;}
  log ('_reportError] exception $exception; stackTrace $stackTrace');}

/*
Future<bool> workmanagerBotPoll() async {
  log ('workmanagerBotPoll] TBD');
  try {
    // TODO: Can we do something useful here,
    //       like waking up the app if there are active entries in the schedule?
  } catch (ex) {log ('...');}
  // NB: Returning `false` stops the workmanager from calling us any further.
  return true;}

void workmanagerCallbackDispatcher() {
  Workmanager.executeTask ((taskName, inputData) async {
    if (taskName == 'botPoll') return await workmanagerBotPoll();
    return false;});}
*/

class BotKey {
  final int key;
  final String secret;
  BotKey (this.key, this.secret);
  BotKey.fromJson (Map<String, dynamic> js): key = js['key'], secret = js['secret'];
  Map<String, dynamic> get toJson => {'key': key, 'secret': secret};}

abstract class BotHandler {
  void botHandle (dynamic struct);
  void botTick();
  Future<BotKey> botKey();}

List<int> netstring (String message) {
  var utf8 = Utf8Encoder();
  var bytes = utf8.convert (message);
  var head = utf8.convert ('${bytes.length}:');
  var tail = utf8.convert (',');
  return head + bytes + tail;}

double now() {return DateTime.now().millisecondsSinceEpoch.toDouble() / 1000.0;}

class Bot {
  BotHandler _botHandler;
  Timer _pingTimer;
  /// The monotonic time of the latest server `_poll` based on the client's time (seconds since UNIX epoch).
  int _requestClock = 0;
  /// The time in seconds when the active `_poll` request was started. 0 when there are no active requests.
  double _requestInitiatedAt = 0;
  /// ETag checksum of the last successfull `_poll` response, initially "0" (Rust u64).
  String _etag = '0';
  static const _BASIC = const BasicMessageChannel ('club.glim.ru/basic', StandardMessageCodec());
  /// A cached key (obtained on demand from BotHandler).
  BotKey _botKey;
  bool botKeyCached = false;

  Future<void> _poll() async {
    try {
      final now = DateTime.now().millisecondsSinceEpoch / 1000;
      // Don't start a new request until a previous request is finished or until its end of life is reached.
      final delta = now - _requestInitiatedAt;
      if (delta.abs() < POLL_WAIT_SEC + 2) {_botHandler.botTick(); return;}

      _requestInitiatedAt = now;
      _requestClock = max (_requestClock + 1, now.floor());
      if (!botKeyCached) {botKeyCached = true; _botKey = await _botHandler.botKey();}
      final res = await botRequest ({
        'poll_wait_sec': POLL_WAIT_SEC,
        'clock': _requestClock,
        'bot_key': _botKey?.toJson
      }, _etag);
      _requestInitiatedAt = 0;  // Allows for a new long-polling request.
      if (res != null) {  // `null` means 304.
        if (res.containsKey('ETag')) _etag = res['ETag'];
        _botHandler.botHandle (res);}
      _botHandler.botTick();

      _BASIC.send ({"foo": 123});  // Example of bus-sending a message to Java.
    } catch (ex) {
      log ('_poll ex: $ex');}}

  Bot (BotHandler bh) {
    _botHandler = bh;
    _BASIC.setMessageHandler ((msg) async {
      // Example of bus-receiving a message from Java.
      //print ('_BASIC, from Java] ' + msg);
    });
    _poll();
    _pingTimer = Timer.periodic (Duration (seconds: 2), (t) => _poll());

    /*
    Workmanager.initialize (workmanagerCallbackDispatcher, isInDebugMode: false);
    Workmanager.registerPeriodicTask ('botPoll', 'botPoll',
      existingWorkPolicy: ExistingWorkPolicy.replace,
      initialDelay: Duration (seconds: 3),
      backoffPolicy: BackoffPolicy.linear,
      backoffPolicyDelay: Duration (seconds: 3));
    */
  }

  void reassemble() {}
  void close() {_pingTimer.cancel();}}
