// https://flutter.dev/docs/cookbook/testing/integration/introduction
// 
//     flutter drive --target=test_driver/main.dart

import 'dart:convert';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  setUpAll (() async {
    driver = await FlutterDriver.connect();});

  tearDownAll (() async {
    if (driver != null) driver.close();});

  group ('home', () {
    test ('has input', () async {
      await driver.waitFor (find.byValueKey ('textInputSection'));});

    test ('use input', () async {
      final input = find.byValueKey ('textInputSection');
      await driver.tap (input);
      final String text = 'Hello world';
      await driver.enterText (text);
      await driver.waitFor (find.text (text));

      // NB: We can enter the text but can't “submit” it, cf. https://github.com/flutter/flutter/issues/9383
      // Using `requestData` to workaround the lack of Enter button in “flutter drive”
      await driver.requestData (json.encode ({
        'method': '_onNewTextInput',
        'text': text}));});});

  group ('notification', () {
    test ('exists', () async {
      // Invoking a helper code in the tested application in order to check Android notifications
      final Map<String, dynamic> got = json.decode (await driver.requestData (json.encode ({
        'method': 'checkIfNotificationExists'})));
      if (got['exists'] != true) throw Exception ('no notification');});});}
